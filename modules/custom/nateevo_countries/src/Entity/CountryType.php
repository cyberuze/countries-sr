<?php
namespace Drupal\nateevo_countries\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Advertiser Type
 * 
 * @ConfigEntityType(
 *   id = "country_type",
 *   label = @Translation("Country Type"),
 *   bundle_of = "country",
 *   entity_keys = {
 *     "id" = "ID",
 *   },
 *   config_prefix = "country_type",
 *   config_export = {
 *     "id",
 *     "label",
 *   },
 *   handlers = {
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer site configuration",
 *   links = {
 *     "canonical" = "/admin/structure/country_type/{country_type}",
 *     "add-form" = "/admin/structure/country_type/add",
 *     "edit-form" = "/admin/structure/country_type/{country_type}/edit",
 *     "delete-form" = "/admin/structure/country_type/{country_type}/delete",
 *     "collection" = "/admin/structure/country_type",
 *   }
 * )
 */
class CountryType extends ConfigEntityBundleBase {}