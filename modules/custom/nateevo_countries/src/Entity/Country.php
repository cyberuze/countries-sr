<?php

namespace Drupal\nateevo_countries\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
	
/**
 * Defines the country entity.
 *
 * @ContentEntityType(
 *   id = "country",
 *   label = @Translation("Country"),
 *   base_table = "nateevo_countries",
 *   entity_keys = {
 *     "id" = "ID",
 *   },
 *   handlers = {
 *     "form" = {
 *       "default" = "Drupal\Core\Entity\ContentEntityForm",
 *       "add" = "Drupal\Core\Entity\ContentEntityForm",
 *       "edit" = "Drupal\Core\Entity\ContentEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   links = {
 *     "canonical" = "/country/{country}",
 *     "add-page" = "/country/add",
 *     "add-form" = "/country/add/{country_type}",
 *     "edit-form" = "/country/{country}/edit",
 *     "delete-form" = "/country/{country}/delete",
 *     "collection" = "/admin/content/countries",
 *   },
 *   admin_permission = "administer site configuration",
 *   bundle_entity_type = "country_type",
 *   field_ui_base_route = "entity.country_type.edit_form",
 * )
 */
class Country extends ContentEntityBase implements ContentEntityInterface {}