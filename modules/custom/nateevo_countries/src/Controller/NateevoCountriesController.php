<?php

namespace Drupal\nateevo_countries\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class NateevoCountriesController extends ControllerBase {
	
	public function printCountriesList(Request $request) {
		
		$url = 'https://restcountries.com/v2/all';		

		$resources = $this->getCountriesDataApi($url);
	
		if(!empty($resources)){
			
			$output = "<table>";
			$output .= "<tr style='font-weight:bold;'>";
			$output .=	"<td>Region</td><td>Flag</td><td>Name</td><td>Capital</td><td>Population</td><td>Density Pop.</td>";
			$output .= "</tr>";
			
			foreach ($resources as $resource) {
				$output .= "<tr style='line-height=50px;'>";
					$output .= "<td>".$resource->region."</td>";					
					$output .= "<td><img src='".$resource->flag."' style='width: 50px; height:auto;margin: 0 18px' /></td>";
					$output .= "<td>".$resource->name."</td>";
					$output .= "<td>".$resource->capital."</td>";
					$output .= "<td>".number_format(floatval($resource->population), 0, ",", ".")."</td>";
					$output .= "<td>".round(intval($resource->population / $resource->area))."</td>";
				$output .= "</tr>";
			}
			$output .= "</table>";
		}
		else {
			$output = "Error getting countries from API";
		}
		return new Response($output);
	}

	public function getCountriesDataApi($url) {
		
		$curl_handle=curl_init();
		
		//This is the URL you would like the content grabbed from

		curl_setopt($curl_handle,CURLOPT_URL, $url);
		
		curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);

		curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
		
		$buffer = curl_exec($curl_handle);
	
		curl_close($curl_handle);
		
		return json_decode($buffer);
	}
	
	public function createCountries(Request $request) {
		
		$type = \Drupal\node\Entity\NodeType::create([
		  'type' => 'country',
		  'name' => 'Country',
		]);

		$type->save();
		
	
		return new Response('Saving countries');
	}

}