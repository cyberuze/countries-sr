<?php

	function nateevo_countries_entity_info() {
		return array(
			'nateevo_countries' => array(
				'label' => 'nateevo-countries',
				'controller class' => 'EntityAPIController',
				'base table' => 'nateevo_countries',
				'entity keys' => array('id' => 'ID'),
			),
		);
	}